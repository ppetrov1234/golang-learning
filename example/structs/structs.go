package main

import (
  "fmt"
)

type person struct {
  name string
  age int
}

func main() {
  p := person{name:"Mike", age:30}
  fmt.Println(p)
  fmt.Println(p.name)
}
