package main

import (
	"fmt"
	"errors"
	"math"
)

func main() {
   //result := sum(2,3)
   //fmt.Println(result)

   result, err := sqrt(-1)
   if err != nil {
     fmt.Println(err)
   } else {
     fmt.Println(result)
   }
}

func sum(x int, y int) int {
  return x + y
}

// Since golang does not have exceptions we need to handle errors in functions

func sqrt(x float64)  (float64, error) {
  if x < 0 {
    return 0, errors.New("Undefined for negative values")
  }

  return math.Sqrt(x), nil	

}

