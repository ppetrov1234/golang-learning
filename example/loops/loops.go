// only loop in golang is FOR loop

package main

import ("fmt")

func main() {
  for i:=1 ; i < 5; i++ {
    fmt.Println(i)
  }

  i := 0
  for i < 5 {
    fmt.Println(i)
    i++
  }

  a := []string{"one", "two", "three"}
  for index, value := range a {
    fmt.Println("index", index, "value", value)
  }

  m := make(map[string]string)
  m["a"] = "one"
  m["b"] = "two"
  for key, value := range m {
    fmt.Println("key:", key, "value:", value)
  }

}
