package main

import (
	"fmt"
)

func main() {
  x := 4
  if x > 5 {
    fmt.Println("x is greater than 5")
  } else if x < 3 {
    fmt.Println("x is not greater than 3")
  } else {
    fmt.Println("I dont know")
  }
}
