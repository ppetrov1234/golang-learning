package main

import (
  "fmt"
)

func main() {
  var a [5]int
  a[2] = 3
  fmt.Println(a)

  b := [5]int{5, 4, 3, 2, 1}
  fmt.Println(b)

  // Slices

  c := []int{}
  c = append(c, 1)
  fmt.Println(c)

  // Maps (Dict or Assoc array in other languages)
  v := make(map[string]int)
  v["first"] = 2
  v["second"] = 3
  fmt.Println(v)
  fmt.Println(v["first"])
  delete(v, "second")
  fmt.Println(v)
}
