// rest-client get articles
// rest-client get article 1
// rest-client del article 1
// rest-client post article {}
// rest-client put article 1

package main

import (
  "log"
  "net/http"
  "io/ioutil"
  "os"
)

func main() {

  route := os.Args[1]
  url := "http://127.0.0.1:10000/"
  get(url,route)

}

func get(url string, route string) {
	path := url + route
	resp, err := http.Get(path)
	if err != nil {
		log.Fatalln("Error:", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln("Error:", err)
	}
	log.Println(string(body))
}


