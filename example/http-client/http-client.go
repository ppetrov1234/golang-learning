// dummy apis
// https://gorest.co.in/
// https://jsonplaceholder.typicode.com/

package main

import (
  "log"
  "net/http"
  "io/ioutil"
)

func main() {
  resp, err := http.Get("https://jsonplaceholder.typicode.com/todos/1")
  if err != nil {
    log.Fatalln("Error:", err)
  } 
  
  defer resp.Body.Close()
  
  body, err := ioutil.ReadAll(resp.Body)

  if err != nil {
    log.Fatalln("Error:", err)
  }
  
  log.Println("Result:", string(body))
  
}
