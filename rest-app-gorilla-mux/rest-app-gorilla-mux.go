// /articles - GET          curl -X GET 127.0.0.1:10000/articles
// /article/{id} - GET      curl -X GET 127.0.0.1:10000/article/1
// /article - POST          curl -X POST -d '{"Id":"3","Title":"Hello 3","desc":"Article Description","content":"Article Content"}' 127.0.0.1:10000/article
// /article/{id} - DELETE   curl -X DELETE 127.0.0.1:10000/article/1
// /article/{id} - PUT      curl -X PUT -d '{"Id":"4","Title":"Hello 4","desc":"Article Description","content":"Article Content"}' 127.0.0.1:10000/article/1

package main

import (
    "fmt"
    "log"
    "net/http"
    "encoding/json"
    "github.com/gorilla/mux"
    "io/ioutil"
)

type Article struct {
    Id string `json:"Id"`
    Title string `json:"Title"`
    Desc string `json:"desc"`
    Content string `json:"content"`
}

// let's declare a global Articles array
// that we can then populate in our main function
// to simulate a database
var articles []Article

func homePage(w http.ResponseWriter, r *http.Request){
    fmt.Fprintf(w, "Welcome to the HomePage!")
    fmt.Println("Endpoint Hit: homePage")
}

func returnAllArticles(w http.ResponseWriter, r *http.Request){
    fmt.Println("Endpoint Hit: returnAllArticles")
    json.NewEncoder(w).Encode(articles)
}

func returnSingleArticle(w http.ResponseWriter, r *http.Request){
    fmt.Println("Endpoint Hit: returnSingleArticle")
    vars := mux.Vars(r)
    key := vars["id"]
//    fmt.Fprintf(w, "Key:" + key)
    for _, article := range articles {
      if article.Id == key {
        json.NewEncoder(w).Encode(article)
      }
    }
}

func createNewArticle(w http.ResponseWriter, r *http.Request) {
    fmt.Println("Endpoint Hit: createNewArticle")
    // get the body of our POST request
    // unmarshal this into a new Article struct
    // append this to our Articles array.    
    reqBody, _ := ioutil.ReadAll(r.Body)
    var article Article 
    json.Unmarshal(reqBody, &article)
    // update our global Articles array to include
    // our new Article
    articles = append(articles, article)

    json.NewEncoder(w).Encode(article)
}

func deleteArticle(w http.ResponseWriter, r *http.Request) {
    fmt.Println("Endpoint Hit: deleteArticle")
    // once again, we will need to parse the path parameters
    vars := mux.Vars(r)
    // we will need to extract the `id` of the article we
    // wish to delete
    id := vars["id"]

    // we then need to loop through all our articles
    for index, article := range articles {
        // if our id path parameter matches one of our
        // articles
        if article.Id == id {
            // updates our Articles array to remove the 
            // article
            articles = append(articles[:index], articles[index+1:]...)
        }
    }

}

func updateArticle(w http.ResponseWriter, r *http.Request) {
    fmt.Println("Endpoint Hit: updateArticle")
    reqBody, _ := ioutil.ReadAll(r.Body)
    var article Article 
    json.Unmarshal(reqBody, &article)
    newarticle := article

    vars := mux.Vars(r)
    id := vars["id"]
    for index, article := range articles {
	    if article.Id == id {
		 fmt.Println(index)
		 articles[index] = newarticle
	    }
    }

}

func handleRequests() {
    r := mux.NewRouter()
    r.HandleFunc("/",homePage)
    r.HandleFunc("/articles",returnAllArticles)
    r.HandleFunc("/article", createNewArticle).Methods("POST")
    r.HandleFunc("/article/{id}", deleteArticle).Methods("DELETE")
    r.HandleFunc("/article/{id}", updateArticle).Methods("PUT")
    r.HandleFunc("/article/{id}",returnSingleArticle).Methods("GET")
    log.Fatal(http.ListenAndServe(":10000", r))
}

func main() {

    articles = []Article{
	    Article{Id: "1", Title: "Hello", Desc: "Article Description", Content: "Article Content"},
	    Article{Id: "2", Title: "Hello 2", Desc: "Article Description", Content: "Article Content"},
    }

    handleRequests()
}
